import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    TextInput,
    Button,
    TouchableOpacity,
    FlatList,
    TouchableHighlight
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class App extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={{ width: '100%', marginLeft: 20, marginRight: 20 }}>
                        <Image source={require('./assets/logo.png')} style={{ width: 375, height: 102 }} />
                    </View>
                    <View style={{ width: '100%', alignItems: 'center' }}>
                        <Text style={{ fontSize: 30, marginTop: 40 }}>Register</Text>
                    </View>
                    <View style={{ width: '100%', alignItems: 'flex-start', marginLeft: 40, marginRight: 40 }}>
                        <Text style={{ marginTop: 40, alignItems: 'flex-start', marginBottom: 5 }}>Username</Text>
                        <TextInput
                            style={{ height: 40, width: '80%', borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => onChangeText(text)}
                        />
                    </View>
                    <View style={{ width: '100%', alignItems: 'flex-start', marginLeft: 40, marginRight: 40 }}>
                        <Text style={{ marginTop: 10, alignItems: 'flex-start', marginBottom: 5 }}>Email</Text>
                        <TextInput
                            style={{ height: 40, width: '80%', borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => onChangeText(text)}
                        />
                    </View>
                    <View style={{ width: '100%', alignItems: 'flex-start', marginLeft: 40, marginRight: 40 }}>
                        <Text style={{ marginTop: 10, alignItems: 'flex-start', marginBottom: 5 }}>Password</Text>
                        <TextInput
                            style={{ height: 40, width: '80%', borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => onChangeText(text)}
                        />
                    </View>
                    <View style={{ width: '100%', alignItems: 'flex-start', marginLeft: 40, marginRight: 40 }}>
                        <Text style={{ marginTop: 10, alignItems: 'flex-start', marginBottom: 5 }}>Ulangi Password</Text>
                        <TextInput
                            style={{ height: 40, width: '80%', borderColor: 'gray', borderWidth: 1 }}
                            onChangeText={text => onChangeText(text)}
                        />
                    </View>
                    <View style={{ width: '100%', alignItems: 'center', marginTop: 20 }}>
                        <TouchableHighlight style={{
                            height: 40, width: '30%',
                            backgroundColor: '#003366',
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor: '#fff',
                            paddingTop: 10,
                            paddingBottom: 10,
                        }}>
                            <Text style={{ color: '#fff', textAlign: 'center' }}>Daftar</Text>
                        </TouchableHighlight>

                        <Text style={{ color: '#3EC6FF', textAlign: 'center', fontSize: 18, marginTop:10, marginBottom:10 }}>Atatu</Text>

                        <TouchableHighlight style={{
                            height: 40, width: '30%',
                            backgroundColor: '#3EC6FF',
                            borderRadius: 20,
                            borderWidth: 1,
                            borderColor: '#fff',
                            paddingTop: 10,
                            paddingBottom: 10,
                        }}>
                            <Text style={{ color: '#fff', textAlign: 'center' }}>Masuk ?</Text>
                        </TouchableHighlight>
                    </View>
                </View>
            </View >
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    navBar: {
        height: 55,
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    rightNav: {
        flexDirection: 'row'
    },
    navItem: {
        marginLeft: 25
    },
    body: {
        flex: 1,
        backgroundColor: 'white',
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    tabItem: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    tabTitle: {
        fontSize: 11,
        color: '#3c3c3c',
        paddingTop: 4
    }
});