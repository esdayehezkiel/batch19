import React from "react";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from "@react-navigation/drawer";
import Login from "./LoginScreen";
import Skill from "./SkillScreen";
import About from "./AboutScreen";
import Project from "./ProjectScreen";
import Add from "./AddScreen";

const Tabs = createBottomTabNavigator();
const RootStack = createStackNavigator();

const TabsScreen = () => (
    < Tabs.Navigator >
        <Tabs.Screen name="Skill" component={Skill} />
        <Tabs.Screen name="Project" component={Project} />
        <Tabs.Screen name="Add" component={Add} />
    </Tabs.Navigator >
);

const Drawer = createDrawerNavigator();
const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Home" component={TabsScreen} />
        <Drawer.Screen name="About" component={About} />
    </Drawer.Navigator>
);

export default () => (
    <NavigationContainer>
        <RootStack.Navigator>
            <RootStack.Screen
                name="Login"
                component={Login}
                options={{ headerShown: false }} />
            <RootStack.Screen
                name="Drawer"
                component={DrawerScreen}
                options={{ headerShown: false }}
            />
        </RootStack.Navigator>
    </NavigationContainer>
);
