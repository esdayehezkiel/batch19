import React, { Component } from 'react';
import { Button, StyleSheet, Text } from 'react-native';
import { ScreenContainer } from 'react-native-screens';
export default class Login extends Component {
    render() {
        return (
            <ScreenContainer style={styles.container}>
                <Text >Halaman Login</Text>
                <Button
                    title="Menuju Halaman Skill"
                    onPress={() => this.props.navigation.navigate(
                        'Drawer', { screen: 'Skill' }
                        // "Skill"
                    )}
                />
            </ScreenContainer>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    }
});