import React, { Component } from 'react';
import { StyleSheet, Text } from 'react-native';
import { ScreenContainer } from 'react-native-screens';
export default class Skill extends Component {
    render() {
        return (
            <ScreenContainer style={styles.container}>
                <Text >Halaman Skill</Text>
            </ScreenContainer>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    }
});