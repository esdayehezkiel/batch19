// import * as React from 'react';
// import { Button, View, Text } from 'react-native';
// import { NavigationContainer } from '@react-navigation/native';
// import { createStackNavigator } from '@react-navigation/stack';

// // Screen Home
// function HomeScreen({ navigation }) {
//     return (
//         <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//             <Text>Home Screen</Text>
//             <Button
//                 title="Go to Details"
//                 onPress={() => navigation.navigate('Details')}
//             />

//             <Button
//                 color="red"
//                 title="Go to Profile"
//                 onPress={() => navigation.navigate('Profiles')}
//             />

//         </View>
//     );
// }

// // Screen Detail
// function DetailsScreen({ route, navigation }) {
//     return (
//         <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//             <Text>Details Screen</Text>
//             <Button
//                 title="Go to Details... again"
//                 onPress={() => navigation.push('Details')}
//             />
//             <Button title="Go to Home" onPress={() => navigation.navigate('Home')} />
//             <Button title="Go back" onPress={() => navigation.goBack()} />
//             <Button
//                 title="Go back to first screen in stack"
//                 onPress={() => navigation.popToTop()}
//             />
//         </View>
//     );
// }

// // Screen Profile
// function ProfilesScreen({ route, navigation }) {
//     return (
//         <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
//             <Text>Profile Screen</Text>
//             <Button
//                 color="black"
//                 title="Go to Home"
//                 onPress={() => navigation.navigate('Home')}
//             />
//             <Button title="Go back" onPress={() => navigation.goBack()} />
//             <Button
//                 color="green"
//                 title="Go back to first screen in stack"
//                 onPress={() => navigation.popToTop()}
//             />
//         </View>
//     );
// }

// // Stack berguna untuk routing aplikasi
// const Stack = createStackNavigator();

// function IndexUI() {
//     return (
//         <NavigationContainer>
//             <Stack.Navigator initialRouteName="Home">
//                 {/* <Stack.Screen name="NamaRute" component={NamaRuteComponent} /> */}
//                 <Stack.Screen name="Home" component={HomeScreen} />
//                 <Stack.Screen name="Details" component={DetailsScreen} />
//                 <Stack.Screen name="Profiles" component={ProfilesScreen} />
//             </Stack.Navigator>
//         </NavigationContainer>
//     );
// }

// export default IndexUI;


import React, { Component } from 'react';
import { View, Text, FlatList, StyleSheet, Image, ActivityIndicator } from 'react-native';
import Axios from 'axios';

export default class   extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      isLoading: true,
      isError: false
    };
  }

  // Mount User Method
  componentDidMount() {
    this.getGithubUser()
  }

  //   Get Api Users
  getGithubUser = async () => {
    try {
      const response = await Axios.get(`https://api.github.com/users?since=135`)
      this.setState({ isError: false, isLoading: false, data: response.data })
    } catch (error) {
      this.setState({ isLoading: false, isError: true })
    }
  }

  render() {
    //  If load data
    if (this.state.isLoading) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <ActivityIndicator size='large' color='red' />
        </View>
      )
    }
    // If data not fetch
    else if (this.state.isError) {
      return (
        <View
          style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
        >
          <Text>Terjadi Error Saat Memuat Data</Text>
        </View>
      )
    }
    // If data finish load
    return (
      <FlatList
        data={this.state.data}
        renderItem={({ item }) =>
          <View style={styles.viewList}>
            <View>
              <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
            </View>
            <View>
              <Text style={styles.textItemLogin}> {item.login}</Text>
              <Text style={styles.textItemUrl}> {item.html_url}</Text>

            </View>
          </View>
        }
        keyExtractor={({ id }, index) => index}
      />
    );
  }
}

const styles = StyleSheet.create({
  viewList: {
    height: 100,
    flexDirection: 'row',
    borderWidth: 1,
    borderColor: '#DDD',
    alignItems: 'center'
  },
  Image: {
    width: 88,
    height: 80,
    borderRadius: 40
  },
  textItemLogin: {
    fontWeight: 'bold',
    textTransform: 'capitalize',
    marginLeft: 20,
    fontSize: 16
  },
  textItemUrl: {
    fontWeight: 'bold',
    marginLeft: 20,
    fontSize: 12,
    marginTop: 10,
    color: 'blue'
  }
})