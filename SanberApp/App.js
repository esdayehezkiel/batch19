/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
// import YoutubeUI from './Tugas/Tugas12/App'
// import SanberUI from './Tugas/Tugas13/App'
// import NoteUI from './Tugas/Tugas14/App'
// import IndexUI from './Tugas/index'
// import NavigationUI from './Tugas/Tugas15/index'
// import Navigation2UI from './Tugas/TugasNavigation/index'
// import Quiz3 from './Tugas/Quiz3';
import AxiosUI from './Tugas/index'


const App = () => {
  return (
    // <YoutubeUI />
    // <SanberUI />
    // <NoteUI />
    // <IndexUI />
    // <NavigationUI />
    // <Navigation2UI />
    // <Quiz3 />
    <AxiosUI />
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
