// SOAL NOMOR 1
var flag = 1;

console.log("LOOPING PERTAMA");
while (flag <= 20) {
    if (flag % 2 == 0) {
        console.log(flag + " I love coding")
    }
    flag++;
}

console.log("LOOPING KEDUA");
while (flag > 0) {
    if (flag % 2 == 0) {
        console.log(flag + " I will become a mobile developer")
    }
    flag--;
}
console.log("-----------------------------------------------------");


// SOAL NOMOR 2

for (let i = 1; i <= 20; i++) {
    if (i % 2 != 0 && i % 3 != 0) {
        console.log(i + " - Santai");
    } else if (i % 3 == 0 && i % 2 != 0) {
        console.log(i + " - I Love Coding");
    } else {
        console.log(i + " - Berkualitas");
    }
}
console.log("-----------------------------------------------------");


// SOAL NOMOR 3
for (let i = 1; i <= 4; i++) {
    var horizontal = "";
    for (let j = 1; j <= 8; j++) {
        horizontal += "#";
    }
    console.log(horizontal);
}
console.log("-----------------------------------------------------");


// SOAL NOMOR 4
var segitiga = "";
for (let i = 1; i <= 7; i++) {
    console.log(segitiga += "#");
}
console.log("-----------------------------------------------------");


// SOAL NOMOR 5
for (let i = 1; i <= 8; i++) {
    var catur = "";
    if (i % 2 == 0) {
        for (let j = 1; j <= 8; j++) {
            if (j % 2 != 0) {
                catur += "#";
            } else {
                catur += " ";
            }
        }
    } else {
        for (let j = 1; j <= 8; j++) {
            if (j % 2 == 0) {
                catur += "#";
            } else {
                catur += " ";
            }
        }
    }
    console.log(catur);
}