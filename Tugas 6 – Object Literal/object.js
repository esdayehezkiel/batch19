// SOAL NOMOR 1
function arrayToObject(arr) {
    var returnData = "";
    for (let i = 0; i < arr.length; i++) {
        var peopleObj = {};
        var now = new Date()
        var thisYear = now.getFullYear() // 2020 (tahun sekarang)
        var age = 0;
        if (arr[i][3] != undefined) {
            age = thisYear - arr[i][3];
            if (age < 0) {
                age = "Invalid birth year";
            }
        } else {
            age = "Invalid birth year";
        }
        peopleObj.firstName = arr[i][0];
        peopleObj.lastName = arr[i][1];
        peopleObj.gender = arr[i][2];
        peopleObj.age = age;
        returnData += (i + 1) + ". " + peopleObj.firstName + " " + peopleObj.lastName + ": " + JSON.stringify(peopleObj) + "\n";
    }
    return returnData;
}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
console.log(arrayToObject(people));


var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
console.log(arrayToObject(people2));

// // Error case 
console.log(arrayToObject([])) // ""

console.log("------------------------------------")


// SOAL NOMOR 2
function shoppingTime(memberId, money) {
    var returnData2 = {};
    var products = {
        "Sepat Stacattu": 1500000,
        "Baju Zoro": 500000,
        "Baju  H&N": 250000,
        "Sweater brand Uniklooh": 175000,
        "Casing Handphone": 50000
    }
    if (memberId == undefined || memberId == "") {
        returnData2 = "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else {
        var listPurchased = [];

        if (money < 50000) {
            returnData2 = "Mohon maaf, uang tidak cukup";
        } else {
            var changeMoney = money;

            Object.keys(products).forEach(key => {
                if (changeMoney - products[key] >= 0) {
                    listPurchased.push(key);
                    changeMoney -= products[key];
                }
            });

            returnData2 = {
                memberId: memberId,
                money: money,
                listPurchased: listPurchased,
                changeMoney: changeMoney
            };

        }
    }
    return returnData2;
}


// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));
//{ memberId: '1820RzKrnWn08',
// money: 2475000,
// listPurchased:
//  [ 'Sepatu Stacattu',
//    'Baju Zoro',
//    'Baju H&N',
//    'Sweater Uniklooh',
//    'Casing Handphone' ],
// changeMoney: 0 }
console.log(shoppingTime('82Ku8Ma742', 170000));
//{ memberId: '82Ku8Ma742',
// money: 170000,
// listPurchased:
//  [ 'Casing Handphone' ],
// changeMoney: 120000 }
console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja
console.log("------------------------------------")


// SOAL NOMOR 3
function naikAngkot(arrPenumpang) {
    var returnData3 = [];
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];

    for (let i = 0; i < arrPenumpang.length; i++) {
        var ongkos = 0;
        ongkos = (rute.indexOf(arrPenumpang[i][2]) - rute.indexOf(arrPenumpang[i][1])) * 2000;

        var returnObj = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: ongkos
        }
        returnData3.push(returnObj);
    }
    return returnData3;
}

//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]

console.log(naikAngkot([])); //[]