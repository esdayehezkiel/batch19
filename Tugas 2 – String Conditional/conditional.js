// SOAL IF ELSE
var nama = "John";
var peran = "";

if (nama == '') {
    // Output untuk Input nama = '' dan peran = ''
    console.log("Nama harus diisi!");
} else {
    if (peran == '') {
        //Output untuk Input nama = 'John' dan peran = ''
        console.log("Halo " + nama + ", Pilih peranmu untuk memulai game!")
    } else if (peran == 'Penyihir') {
        //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Penyihir " + nama + ", kamu dapat melihat siapa yang menjadi werewolf!");
    } else if (peran == 'Guard') {
        //Output untuk Input nama = 'Jenita' dan peran 'Guard'
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Guard " + nama + ", kamu akan membantu melindungi temanmu dari serangan werewolf.");
    } else if (peran == 'Werewolf') {
        //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Halo Werewolf " + nama + ", Kamu akan memakan mangsa setiap malam!");
    } else {
        console.log("Selamat datang di Dunia Werewolf, " + nama);
        console.log("Peran yang kamu pilih tidak ada! Pilihlah salah satu peran sebagai Penyihir, Guard, Atau Werewolf!");
    }
}
console.log('----------------------------------------');


// SOAL SWITCH CASE

var tanggal = 4; // assign nilai variabel tanggal disini! (dengan angka antara 1 - 31)
var bulan = 11; // assign nilai variabel bulan disini! (dengan angka antara 1 - 12)
var tahun = 2020; // assign nilai variabel tahun disini! (dengan angka antara 1900 - 2200)
//  Maka hasil yang akan tampil di console adalah: '4 November 2020';
var namaBulan = "";

switch (bulan) {
    case 1: { namaBulan = "Januari"; break; }
    case 2: { namaBulan = "Februari"; break; }
    case 3: { namaBulan = "Maret"; break; }
    case 4: { namaBulan = "April"; break; }
    case 5: { namaBulan = "Mei"; break; }
    case 6: { namaBulan = "Juni"; break; }
    case 7: { namaBulan = "Juli"; break; }
    case 8: { namaBulan = "Agustus"; break; }
    case 9: { namaBulan = "September"; break; }
    case 10: { namaBulan = "Oktober"; break; }
    case 11: { namaBulan = "November"; break; }
    case 12: { namaBulan = "Desember"; break; }
    default: { console.log('Tidak terjadi apa-apa'); }
}

console.log(tanggal + ' ' + namaBulan + ' ' + tahun);