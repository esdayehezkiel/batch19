// SOAL NOMOR 1
function range(startNum, finishNum) {
    var array = []
    if (startNum == undefined || finishNum == undefined) {
        return -1;
    }
    if (startNum < finishNum) {
        for (startNum; startNum <= finishNum; startNum++) {
            array.push(startNum);
        }
        return array;
    } else {
        for (startNum; startNum >= finishNum; startNum--) {
            array.push(startNum);
        }
        return array;
    }
}
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11, 18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 
console.log("--------------------------------------------")


// SOAL NOMOR 2
function rangeWithStep(startNum, finishNum, step) {
    var array = []
    if (startNum == undefined || finishNum == undefined || step == undefined) {
        return -1;
    }
    if (startNum < finishNum) {
        for (startNum; startNum <= finishNum; startNum += step) {
            array.push(startNum);
        }
        return array;
    } else {
        for (startNum; startNum >= finishNum; startNum -= step) {
            array.push(startNum);
        }
        return array;
    }
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 
console.log("--------------------------------------------")


// SOAL NOMOR 3
function sum(startNum, finishNum = 1, step = 1) {
    var sumValue = 0;
    if (startNum == undefined && finishNum == undefined && step == undefined) {
        return 0;
    }
    if (startNum < finishNum) {
        for (startNum; startNum <= finishNum; startNum += step) {
            sumValue += startNum;
        }
        return sumValue;
    } else {
        for (startNum; startNum >= finishNum; startNum -= step) {
            sumValue += startNum;
        }
        return sumValue;
    }
}

console.log(sum(1, 10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15, 10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log("--------------------------------------------")


// SOAL NOMOR 4
//contoh input
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]

function dataHandling(inputValue) {
    for (let index = 0; index < inputValue.length; index++) {
        console.log("Nomor ID: " + input[index][0])
        console.log("Nama Lengkap: " + input[index][1])
        console.log("TTL: " + input[index][2] + " " + input[index][3])
        console.log("Hobi: " + input[index][4])
        console.log("");
    }
}

dataHandling(input)
console.log("--------------------------------------------")


// SOAL NOMOR 5
function balikKata(kata) {
    var terbalik = "";
    var max = kata.length - 1;
    for (let i = max; i >= 0; i--) {
        terbalik += kata[i];
    }
    return terbalik;
}
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 
console.log("--------------------------------------------")


// SOAL NOMOR 6
var input6 = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(param) {

    param.splice(1, 4, "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro")
    console.log(param)

    var tanggalSplit = param[3].split("/");
    var namaBulan = "";
    switch (tanggalSplit[1]) {
        case "01": { namaBulan = "Januari"; break; }
        case "02": { namaBulan = "Februari"; break; }
        case "03": { namaBulan = "Maret"; break; }
        case "04": { namaBulan = "April"; break; }
        case "05": { namaBulan = "Mei"; break; }
        case "06": { namaBulan = "Juni"; break; }
        case "07": { namaBulan = "Juli"; break; }
        case "08": { namaBulan = "Agustus"; break; }
        case "09": { namaBulan = "September"; break; }
        case "10": { namaBulan = "Oktober"; break; }
        case "11": { namaBulan = "November"; break; }
        case "12": { namaBulan = "Desember"; break; }
        default: { console.log('Tidak terjadi apa-apa'); }
    }
    console.log(namaBulan);

    var tanggalSplitDesc = tanggalSplit.slice();
    tanggalSplitDesc.sort(function (value1, value2) { return value2 - value1 });
    console.log(tanggalSplitDesc)

    console.log(tanggalSplit.join("-"))

    var namaPotong = param[1].slice(0, 14);
    console.log(namaPotong)
}
dataHandling2(input6);

/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */