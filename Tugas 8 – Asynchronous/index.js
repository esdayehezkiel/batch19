// di index.js
var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Tulis code untuk memanggil function readBooks di sini
var waktu = 10000;
var i = 0;

readBooks(waktu, books[i], function (sisaWaktu) {
    i++;
    readBooks(sisaWaktu, books[i], function (sisaWaktu2) {
        i++;
        readBooks(sisaWaktu2, books[i], function (sisaWaktu3) {
           console.log("-------------------------------------------------")
        })
    })
})

