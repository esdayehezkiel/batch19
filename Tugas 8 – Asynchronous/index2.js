var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

// Lanjutkan code untuk menjalankan function readBooksPromise 
var waktu = 10000;
var i = 0;
readBooksPromise(waktu, books[i])
    .then(function (sisaWaktu) {
        i++;
        readBooksPromise(sisaWaktu, books[i])
            .then(function (sisaWaktu2) {
                i++
                readBooksPromise(sisaWaktu2, books[i])
            })
            .catch(function (sisaWaktu2) {
                console.log(sisaWaktu2)
            })
    })
    .catch(function (sisaWaktu) {
        console.log(sisaWaktu)
    })
