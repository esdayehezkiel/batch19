import React, { Component, useRef, useState, useEffect } from 'react';
import {
    View,
    Text,
    Dimensions,
    StyleSheet,
    TouchableOpacity,
    Platform,
    SafeAreaView,
    Alert,
    Button
} from 'react-native';
import { ScreenContainer } from 'react-native-screens';
import Axios from 'axios';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

// import { Card, ListItem, Button, Icon } from 'react-native-elements'

const { width: screenWidth } = Dimensions.get('window')

const SLIDERS = [
    {
        title: 'Beautiful and dramatic Antelope Canyon',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://stoppneumonia.id/wp-content/uploads/2020/03/capturevido-corona.png',
    },
    {
        title: 'White Pocket Sunset',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
        illustration: 'https://static.vecteezy.com/system/resources/previews/000/952/693/non_2x/prevention-of-covid-19-concept-vector.jpg',
    },
    {
        title: 'Earlier this morning, NYC',
        subtitle: 'Lorem ipsum dolor sit amet',
        illustration: 'https://static.vecteezy.com/system/resources/previews/000/834/908/non_2x/poster-with-virus-protection-icons-and-man-working-from-home-vector.jpg',
    },
    {
        title: 'Acrocorinth, Greece',
        subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
        illustration: 'https://static.vecteezy.com/system/resources/previews/000/833/601/non_2x/stop-covid-19-template-banner-vector.jpg',
    }
];


export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataIndonesia: {},
            isLoading: true,
            isError: false,
            entries: SLIDERS,
        };
    }

    // SLIDER
    _renderItem({ item, index }, parallaxProps) {
        return (
            <View style={styles.item}>
                <ParallaxImage
                    source={{ uri: item.illustration }}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
                {/* <Text style={styles.title} numberOfLines={2}>
                    {item.title}
                </Text> */}
            </View>
        );
    }
    // SLIDER

    componentDidMount() {
        this.getGlobalPositif()
    }
    getGlobalPositif = async () => {
        try {
            const response = await Axios.get(`https://api.kawalcorona.com/indonesia`)
            this.setState({ isError: false, isLoading: false, dataIndonesia: response.data[0] })
            console.log('DATA INDO: ', response.data);
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    render() {
        return (
            <ScreenContainer style={styles.container}>
                <View style={{
                    flexDirection: "row",
                    height: 250,
                    padding: 5,
                    marginLeft: -40,
                    marginRight: 10,
                    marginTop: 20
                }}>
                    {/* <View style={{ backgroundColor: "blue", flex: 0.3 }} />
                    <View style={{ backgroundColor: "red", flex: 0.5 }} />
                    <Text>{this.state.dataPositif.value}</Text> */}
                    <Carousel
                        sliderWidth={screenWidth}
                        sliderHeight={screenWidth}
                        itemWidth={screenWidth - 100}
                        data={this.state.entries}
                        renderItem={this._renderItem}
                        hasParallaxImages={true}
                    />
                </View>
                <View
                    style={{
                        flexDirection: "row",
                        height: 30,
                        marginLeft: 20,
                        marginRight: 10,
                    }}>
                    <View style={{ flex: 0.7, backgroundColor: "white" }}>
                        <Text style={{ color: '#0d0c0c', fontSize: 20 }}>Butuh Bantuan Segera?</Text>
                    </View>
                    <View style={{ flex: 0.3, paddingRight: 5 }}>
                        <Button
                            title="Telepon"
                            color="#f55d2f"
                            onPress={() => Alert.alert('Telepon 1500040')}
                        />
                    </View>
                </View>

                <View style={{
                    flexDirection: "row",
                    height: 100,
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 20
                }}>
                    <TouchableOpacity style={{ backgroundColor: '#578ade', width: '98%', borderRadius: 10 }}>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 18, fontWeight: "bold", color: "white" }} >Positif</Text>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 28, fontWeight: "bold", color: "white" }} >{this.state.dataIndonesia.positif}</Text>
                    </TouchableOpacity>
                </View>
                <View style={{
                    flexDirection: "row",
                    height: 100,
                    marginLeft: -7,
                    marginRight: -7,
                    marginTop: 10
                }}>
                    <TouchableOpacity style={{ backgroundColor: '#fcab14', borderRadius: 10, flex: 0.3, marginRight: 10 }}>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 15, fontWeight: "bold", color: "white" }}>Dirawat</Text>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 25, fontWeight: "bold", color: "white" }} >{this.state.dataIndonesia.dirawat}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: '#41c466', borderRadius: 10, flex: 0.3, marginRight: 10 }}>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 15, fontWeight: "bold", color: "white" }}>Sembuh</Text>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 25, fontWeight: "bold", color: "white" }} >{this.state.dataIndonesia.sembuh}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ backgroundColor: '#ff5757', borderRadius: 10, flex: 0.3 }}>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 15, fontWeight: "bold", color: "white" }}>Meninggal</Text>
                        <Text style={{ marginLeft: 20, marginTop: 5, fontSize: 25, fontWeight: "bold", color: "white" }} >{this.state.dataIndonesia.meninggal}</Text>
                    </TouchableOpacity>
                </View>

            </ScreenContainer>
        );
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
    },

    item: {
        width: screenWidth - 100,
        height: screenWidth - 200,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },
    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'center',
    },
    title: {
        // marginTop:-80
    }
});