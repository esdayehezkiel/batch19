import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, TextInput, Alert, Button } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { ScreenContainer } from 'react-native-screens';
export default class Account extends Component {

    render() {
        return (
            <ScreenContainer style={styles.container}>

                <View style={{ justifyContent: 'flex-start', alignItems: 'center', flex: 1 }}>
                    <Image
                        style={styles.tinyLogo}
                        source={require('./../assets/logo.png')}
                    />
                    <Text style={{ fontSize: 25, fontWeight: 'bold', color: '#262c76', margin: 20 }}>Selamat Datang</Text>
                    <TextInput
                        style={{ height: 50, borderColor: 'rgba(161, 161, 161, 0.7)', borderWidth: 1, borderRadius: 5, width: 300, paddingLeft: 10, margin: 20 }}
                        onChangeText={text => onChangeText(text)} placeholder="Email"
                    />
                    <TextInput
                        style={{ height: 50, borderColor: 'rgba(161, 161, 161, 0.7)', borderWidth: 1, borderRadius: 5, width: 300, paddingLeft: 10 }}
                        onChangeText={text => onChangeText(text)} placeholder="Password"
                    />
                    <Text style={{ textAlign: 'right', alignSelf: 'stretch', marginRight: 22, marginTop: 5, marginBottom: 35 }} onPress={() => Alert.alert('Cek email, link reset sudah dikirim.')}>Lupa Password?</Text>

                    <TouchableOpacity style={{ backgroundColor: "#262c76", borderRadius: 5, justifyContent: 'center', alignItems: "center", width: 300, height: 45 }}>
                        <Text style={{ color: "white", fontWeight: 'bold', letterSpacing: 2 }}>LOGIN</Text>
                    </TouchableOpacity>
                    <View
                        style={{
                            flexDirection: "row",
                            padding: 20
                        }}
                    >
                        <Text>Belum ada akun? </Text>
                        <Text style={{ color: "#262c76", fontWeight: 'bold', textDecorationLine: 'underline'}}  onPress={() => Alert.alert('Daftar Yuk')}>Daftar Sekarang</Text>
                    </View>
                </View>

            </ScreenContainer>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    tinyLogo: {
        marginTop: 40,
        width: 140,
        height: 140,
    },
});