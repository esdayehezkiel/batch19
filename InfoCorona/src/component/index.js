import React, { useEffect } from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { createDrawerNavigator } from "@react-navigation/drawer";
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Splash from './Splash'
import Home from './Home';
import About from './About';
import Province from './Province';
import Account from './Account';
import Detail from './Detail';

const RootStack = createStackNavigator();
const Tabs = createBottomTabNavigator();
const TabsScreen = () => (
    < Tabs.Navigator tabBarOptions={{
        activeBackgroundColor: '#262c76',
        inactiveBackgroundColor: '#262c76',
        activeTintColor: '#ffbf5e',
    }} >
        <Tabs.Screen
            name="Beranda"
            component={Home}
            options={{
                tabBarIcon: ({ focused, tintColor }) => (
                    focused == true ? <MaterialCommunityIcons name="home" color={"#ffbf5e"} size={25} /> : <MaterialCommunityIcons name="home" color={"white"} size={25} />
                ),
                // tabBarBadge: 3,
            }}
        />
        <Tabs.Screen
            name="Daftar Provinsi"
            component={Province}
            options={{
                tabBarIcon: ({ focused, tintColor }) => (
                    focused == true ? <MaterialCommunityIcons name="clipboard-list" color={"#ffbf5e"} size={25} /> : <MaterialCommunityIcons name="clipboard-list" color={"white"} size={25} />
                ),
                // tabBarBadge: 3,
            }}
        />
        <Tabs.Screen
            name="Akun Saya"
            component={Account}
            options={{
                tabBarIcon: ({ focused, tintColor }) => (
                    focused == true ? <MaterialCommunityIcons name="account" color={"#ffbf5e"} size={25} /> : <MaterialCommunityIcons name="account" color={"white"} size={25} />
                ),
            }}
        />
        <Tabs.Screen
            name="Tentang"
            component={About}
            options={{
                tabBarIcon: ({ focused, tintColor }) => (
                    focused == true ? <MaterialCommunityIcons name="information" color={"#ffbf5e"} size={25} /> : <MaterialCommunityIcons name="information" color={"white"} size={25} />
                ),
            }}
        />
    </Tabs.Navigator >
);

const IndexUI = () => {
    // return <Splash />
    return (
        <NavigationContainer>
            <RootStack.Navigator>
                <RootStack.Screen
                    name="Splash"
                    component={Splash}
                    options={{ headerShown: false }} />
                <RootStack.Screen
                    name="Tabs"
                    component={TabsScreen}
                    options={{ headerShown: false }}
                />
                <RootStack.Screen
                    name="Detail"
                    component={Detail}
                    options={({ route }) => ({ title: route.params.title })}
                />
            </RootStack.Navigator>
        </NavigationContainer>
    );
}

export default IndexUI

const styles = StyleSheet.create({
    tinyLogo: {
        width: 150,
        height: 150,
    },
})
