import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView, Image, Dimensions, TouchableOpacity } from 'react-native';
import {
    PieChart
} from "react-native-chart-kit";

const chartConfig = {
    backgroundGradientFrom: "#1E2923",
    backgroundGradientFromOpacity: 0,
    backgroundGradientTo: "#08130D",
    backgroundGradientToOpacity: 0.5,
    color: (opacity = 1) => `rgba(26, 255, 146, ${opacity})`,
    strokeWidth: 2, // optional, default 3
    barPercentage: 0.5,
    useShadowColorFromDataset: false, // optional

};

export default class Detail extends Component {
    constructor(props) {
        super(props);
        console.log('\n', this.props.route.params.item.jenis_kelamin,)
        this.state = {
            dataJenisKelamin: this.props.route.params.item.jenis_kelamin,
            dataKelompokUmur: this.props.route.params.item.kelompok_umur,
            dataPenambahan: this.props.route.params.item.penambahan
        };

    }

    render() {

        let dataBaru = [
            {
                name: '| ' + this.state.dataJenisKelamin[0].key,
                population: this.state.dataJenisKelamin[0].doc_count,
                color: "rgba(4, 0, 255, 0.78)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| ' + this.state.dataJenisKelamin[1].key,
                population: this.state.dataJenisKelamin[1].doc_count,
                color: "rgba(4, 0, 255, 0.54)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            }
        ];
        let dataUmur = [
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[0].key,
                population: this.state.dataKelompokUmur[0].doc_count,
                color: "rgba(4, 0, 255, 0.20)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[1].key,
                population: this.state.dataKelompokUmur[1].doc_count,
                color: "rgba(165, 230, 186, 1)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[2].key,
                population: this.state.dataKelompokUmur[2].doc_count,
                color: "rgba(192, 248, 209, 1)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[3].key,
                population: this.state.dataKelompokUmur[3].doc_count,
                color: "rgba(94, 177, 191, 1)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[4].key,
                population: this.state.dataKelompokUmur[4].doc_count,
                color: "rgba(111, 156, 235, 1)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            },
            {
                name: '| Umur: ' + this.state.dataKelompokUmur[5].key,
                population: this.state.dataKelompokUmur[5].doc_count,
                color: "rgba(4, 0, 255, 0.70)",
                legendFontColor: "#7F7F7F",
                legendFontSize: 13
            }
        ];
        return (

            <ScrollView style={{ flex: 1, padding: 10 }}>
                <PieChart
                    data={dataBaru}
                    width={400}
                    height={220}
                    chartConfig={chartConfig}
                    accessor="population"
                    backgroundColor="rgba(223, 210, 159, 0.82)"
                    style={{ borderRadius: 10, marginBottom: 20 }}
                    paddingLeft="15"
                    absolute
                />
                <PieChart
                    data={dataUmur}
                    width={400}
                    height={220}
                    chartConfig={chartConfig}
                    accessor="population"
                    backgroundColor="rgba(223, 210, 159, 0.82)"
                    style={{ borderRadius: 10 }}
                    paddingLeft="15"
                    absolute
                />
                <TouchableOpacity
                    style={styles.item}>
                    <View style={{
                        flexDirection: "row",
                    }}>
                        <Text style={{ marginLeft: "auto", marginRight: 'auto', fontSize: 20 }}>Penambahan Hari Ini</Text>
                    </View>
                    <View
                        style={{
                            flexDirection: "row",
                            padding: 20
                        }}
                    >
                        <Text style={{ flex: 0.3 }}>Positif: {this.state.dataPenambahan.positif}</Text>
                        <Text style={{ flex: 0.3 }}>Sembuh: {this.state.dataPenambahan.sembuh}</Text>
                        <Text style={{ flex: 0.3 }}>Meninggal: {this.state.dataPenambahan.meninggal}</Text>
                    </View>
                </TouchableOpacity>
            </ScrollView>



        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    overlay: {
        backgroundColor: 'rgba(7, 1, 39, 0.91)',
        height: '100%',
        width: '100%'
    },
    item: {
        borderRadius: 10,
        padding: 10,
        marginVertical: 8,
        borderWidth: 1,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        width: '99%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,

    },
});