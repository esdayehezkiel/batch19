import React, { Component } from 'react';
import { StyleSheet, Text, View, ImageBackground, ScrollView, Image } from 'react-native';
import { ScreenContainer } from 'react-native-screens';
export default class About extends Component {
    render() {
        return (
            <View style={{ flex: 1 }}>
                <ImageBackground
                    resizeMode={'cover'} // or cover
                    style={{ flex: 1 }} // must be passed from the parent, the number may vary depending upon your screen size
                    source={require('./../assets/about.jpg')}
                >
                    <View style={styles.overlay}>
                        <ScrollView>
                            {/*Render the children here*/}
                            <Image
                                source={require('./../assets/profile.jpg')}
                                style={{ width: 120, height: 120, borderRadius: 120 / 2, marginLeft: 'auto', marginRight: 'auto', marginTop: 50, zIndex: 9 }}
                            />
                            <View
                                style={{
                                    // flexDirection: "row",
                                    height: 400,
                                    padding: 20,
                                    backgroundColor: "rgba(255, 255, 255, 0.84)",
                                    flex: 1,
                                    borderRadius: 10,
                                    marginTop: -60,
                                    zIndex: 1
                                }}
                            >
                                <Text style={{ marginTop: 50, marginLeft: 'auto', marginRight: 'auto', fontSize: 18, marginBottom: 20 }}>Esda Yehezkiel E Harinanto</Text>
                                <View
                                    style={{
                                        // flexDirection: "row",
                                        height: 70,
                                        alignItems: 'center'
                                    }}
                                >
                                    <Image
                                        source={require('./../assets/logo-gitlab.jpg')}
                                        style={{ width: 40, height: 40, borderRadius: 10, marginLeft: 10, marginRight: 10 }}
                                    />
                                    <Text >@esdayehezkiel</Text>
                                </View>
                                <View
                                    style={{
                                        // flexDirection: "row",
                                        height: 100,
                                        alignItems: 'center'
                                    }}
                                >
                                    <Image
                                        source={require('./../assets/logo-linkedin.png')}
                                        style={{ width: 40, height: 40, borderRadius: 10, marginLeft: 10, marginRight: 10 }}
                                    />
                                    <Text >@esdayehezkiel</Text>
                                </View>
                                <View style={styles.separator} />
                                <Text style={{margin:10, textAlign: 'center', fontWeight:'bold'}}>Aplikasi InfoCorona ini dibuat sebagai tugas akhir mengikuti training {"\n"} #IndonesiaMengoding dari #Sanbercode {"\n"}dalam kelas #ReactNativeDasar </Text>

                            </View>
                        </ScrollView>
                    </View>
                </ImageBackground>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#fff",
        alignItems: "center",
        justifyContent: "center"
    },
    overlay: {
        backgroundColor: 'rgba(7, 1, 39, 0.91)',
        height: '100%',
        width: '100%'
    },
    separator: {
        marginVertical: 8,
        borderBottomColor: '#737373',
        borderBottomWidth: StyleSheet.hairlineWidth,
    },
});