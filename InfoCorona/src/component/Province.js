import React, { Component } from 'react';
import { StyleSheet, Text, FlatList, TouchableHighlight, View, StatusBar, TouchableOpacity } from 'react-native';
import { ScreenContainer } from 'react-native-screens';
import Axios from 'axios';

export default class Province extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading: true,
            isError: false,
            dataProvinsiIndonesia: {}
        };
    }

    // Mount CALL
    componentDidMount() {
        this.getProvinsiIndonesia()
    }

    //   Get Api PROVINCE
    getProvinsiIndonesia = async () => {
        try {
            const response = await Axios.get(`https://data.covid19.go.id/public/api/prov.json`)
            this.setState({ isError: false, isLoading: false, dataProvinsiIndonesia: response.data.list_data })
            // console.log('PROVINCE', response.data);
        } catch (error) {
            this.setState({ isLoading: false, isError: true })
        }
    }

    formatRibuan(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    }

    render() {
        return (
            <ScreenContainer style={styles.container}>
                <View style={{ marginTop: 10, marginBottom: 10 }}>
                    <Text style={{ fontSize: 24, fontWeight: "bold", marginLeft: 'auto', marginRight: 'auto' }}>DATA PER PROVINSI DI INDONESIA</Text>
                    <Text style={{ textAlign: 'center', fontStyle: 'italic' }}>(Klik kartu untuk lihat detail)</Text>
                </View>
                <FlatList
                    ItemSeparatorComponent={
                        Platform.OS !== 'android' &&
                        (({ highlighted }) => (
                            <View
                                style={[
                                    style.separator,
                                    highlighted && { marginLeft: 0 }
                                ]}
                            />
                        ))
                    }
                    data={this.state.dataProvinsiIndonesia}
                    renderItem={({ item, index, separators }) => (
                        <TouchableOpacity
                            onPress={() => this.props.navigation.navigate(
                                "Detail",
                                {
                                    item: item,
                                    title: 'DETAIL PROVINSI ' + item.key
                                }
                            )}
                            style={styles.item}>
                            <Text style={styles.title}>{item.key}</Text>
                            <View style={{
                                flexDirection: "row",
                                height: 100,
                                padding: 20
                            }}>
                                <View style={styles.detail}>
                                    <Text style={styles.textCenter}>Positif</Text>
                                    <Text style={styles.positif}>{this.formatRibuan(item.jumlah_kasus)}</Text>
                                </View>
                                <View style={styles.detail}>
                                    <Text style={styles.textCenter}>Sembuh</Text>
                                    <Text style={styles.sembuh}>{this.formatRibuan(item.jumlah_sembuh)}</Text>
                                </View>
                                <View style={styles.detail}>
                                    <Text style={styles.textCenter}>Meninggal</Text>
                                    <Text style={styles.meninggal}>{this.formatRibuan(item.jumlah_meninggal)}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>

                    )}
                    keyExtractor={(item) => item.key}
                />

            </ScreenContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight || 0,
    },
    item: {
        padding: 20,
        marginVertical: 8,
        // marginHorizontal: 16,
        borderWidth: 1,
        borderColor: '#ffffff',
        backgroundColor: '#ffffff',
        // elevation: 1,
        width: '100%',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3,
        },
        shadowOpacity: 0.27,
        shadowRadius: 4.65,
        elevation: 6,

    },
    title: {
        fontSize: 20,
        marginLeft: 'auto',
        marginRight: 'auto',
        fontWeight: 'bold'
    },
    detail: {
        flex: 0.3,
        // backgroundColor: 'red', 
        justifyContent: 'center',
    },
    textCenter: {
        textAlign: 'center',
    },
    positif: {
        textAlign: 'center',
        fontSize: 18,
        color: 'blue'
    },
    sembuh: {
        textAlign: 'center',
        fontSize: 18,
        color: 'green'
    },
    meninggal: {
        textAlign: 'center',
        fontSize: 18,
        color: 'red'
    }
});